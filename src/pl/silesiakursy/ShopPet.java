package pl.silesiakursy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class ShopPet {
    private List<Pet> pets;
    Scanner scanner = new Scanner(System.in);

    public ShopPet() {
        this.pets = new ArrayList<>();
        pets.add(new Pet("pies", "czarny", 100.10f));
        pets.add(new Pet("szynszyla", "brąz", 25.50f));
        pets.add(new Pet("kret", "czarny", 12.89f));
    }

    public List<Pet> getPets() {
        return pets;
    }

    public void setPets(Pet pet) {
        pets.add(pet);
    }

    public void addPetForSell() {
        //TODO try...catch
        System.out.println("Podj gatunek;");
        String gatunek = scanner.nextLine();
        System.out.println("Podaj kolor");
        String kolor = scanner.nextLine();
        System.out.println("Podaj cenę");
        float cena = scanner.nextFloat();
        scanner.nextLine();

        setPets(new Pet(gatunek, kolor, cena));
    }

    public void showPetsForSell() {
//        for (Pet pet : pets) {
//            System.out.println(pet);
//        }
        System.out.println(pets);
    }

    public void sellPet() {
        //TODO try...catch
        System.out.println("Podaj UUID zwierzaka do sprzedaży:");
        String uuid = scanner.nextLine();
        removePetByUUID(UUID.fromString(uuid.trim()));
    }

    public void showBalance() {
        float balance = 0;
        for (Pet pet : pets) {
            balance += pet.getCena();
        }
        System.out.println("Bilans sklepu: " + balance);
    }

    private void removePetByUUID(UUID uuid) {
        pets.remove(findPetByUUID(uuid));
    }

    public Pet findPetByUUID(UUID uuid) {
        for (Pet pet : pets) {
            if (pet.getUuid().equals(uuid)) {
                return pet;
            }
        }
        return null;
    }

    public List<Pet> findPetBySpecie(String specie) {
        List<Pet> petsBySpecie = new ArrayList<>();

        for (Pet pet : pets) {
            if (pet.getGatunek().equals(specie)) {
                petsBySpecie.add(pet);
            }
        }
        return petsBySpecie;
    }

    public List<Pet> findPetByColor(String color) {
        List<Pet> petsByColor = new ArrayList<>();

        for (Pet pet : pets) {
            if (pet.getKolor().equals(color)) {
                petsByColor.add(pet);
            }
        }
        return petsByColor;
    }

    public List<Pet> findPetByPrice(float price) {
        List<Pet> petsByPrice = new ArrayList<>();

        for (Pet pet : pets) {
            if (pet.getCena()==price) {
                petsByPrice.add(pet);
            }
        }
        return petsByPrice;
    }
}