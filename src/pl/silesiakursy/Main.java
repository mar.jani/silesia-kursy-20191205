package pl.silesiakursy;

import java.util.Scanner;
import java.util.UUID;

public class Main {
    static ShopPet shopPet = new ShopPet();
    static Menu mainMenu = new Menu();
    static SearchMenu searchMenu = new SearchMenu();

    public static void main(String[] args) {
        mainGui();
    }

    private static void mainGui() {
        int menuChoice = mainMenu.showMenuAndReturnChoice();

        if (menuChoice == 1) {
            shopPet.addPetForSell();
            mainGui();
        } else if (menuChoice == 2) {
            shopPet.sellPet();
            mainGui();
        } else if (menuChoice == 3) {
            shopPet.showPetsForSell();
            mainGui();
        } else if (menuChoice == 4) {
            searchMenuGui(searchMenu.showSearchMenuAndReturnChoice());
            mainGui();
        } else if (menuChoice == 5) {
            shopPet.showBalance();
            mainGui();
        } else {
            System.out.println("Błędna wartość");
            mainGui();
        }
    }

    private static void searchMenuGui(int searchMenuChoice) {
        if (searchMenuChoice == 1) {
            findByUUID();
            mainGui();
        } else if (searchMenuChoice == 2) {
            findBySpecie();
            mainGui();
        } else if (searchMenuChoice == 3) {
            findByColor();
            mainGui();
        } else if (searchMenuChoice == 4) {
            findByPrice();
            mainGui();
        } else {
            mainGui();
        }
    }

    private static void findByUUID() {
        System.out.println("Podaj UUID: ");
        String uuid = inputStringToFind();
        System.out.println(shopPet.findPetByUUID(UUID.fromString(uuid)));
    }

    private static void findBySpecie() {
        System.out.println("Podaj gatunek: ");
        String specie = inputStringToFind();
        System.out.println(shopPet.findPetBySpecie(specie));
    }

    private static void findByColor() {
        System.out.println("Podaj kolor: ");
        String color = inputStringToFind();
        System.out.println(shopPet.findPetByColor(color));
    }

    private static String inputStringToFind() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private static void findByPrice() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj cenę: ");
        float price = scanner.nextFloat();
        System.out.println(shopPet.findPetByPrice(price));
    }
}
