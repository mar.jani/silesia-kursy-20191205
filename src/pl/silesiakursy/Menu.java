package pl.silesiakursy;

import java.util.Scanner;

public class Menu {
    public int showMenuAndReturnChoice() {
        System.out.println("=================MENU===========================");
        System.out.println("1. Dodaj zwierzaka");
        System.out.println("2. Sprzedaj zwierzaka");
        System.out.println("3. Lista zwierzaków na sprzedaż");
        System.out.println("4. Wyszukiwarka");
        System.out.println("5. Wyświetl bilans sklepu");
        System.out.println("================================================");

        Scanner scanner = new Scanner(System.in);

        int menuChoice = 0;
        try {
            menuChoice = scanner.nextInt();
            scanner.nextLine();
            if (menuChoice < 1 || menuChoice > 5) {
                throw new Exception("Wybierz prawidłową pozycję z menu");
            }
        } catch (Exception e) {
            System.out.println("Wybierz prawidłową pozycję z menu");
        }
        return menuChoice;
    }
}
