package pl.silesiakursy;

import java.util.UUID;

public class Pet {
    private UUID uuid;
    private String gatunek;
    private String kolor;
    private float cena;

    public Pet() {
    }

    public Pet(String gatunek, String kolor, float cena) {
        this.uuid = UUID.randomUUID();
        this.gatunek = gatunek;
        this.kolor = kolor;
        this.cena = cena;
    }

    public String getGatunek() {
        return gatunek;
    }

    public void setGatunek(String gatunek) {
        this.gatunek = gatunek;
    }

    public String getKolor() {
        return kolor;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public float getCena() {
        return cena;
    }

    public void setCena(float cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return uuid.toString() + "\t" + getGatunek() + "\t" + getKolor() + "\t" + getCena();
    }
}
