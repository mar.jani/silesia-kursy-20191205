package pl.silesiakursy;

import java.util.Scanner;

public class SearchMenu {
    public int showSearchMenuAndReturnChoice(){
        System.out.println("==================Wyszukiwarka===================");
        System.out.println("1. Wyszukaj po Uuid");
        System.out.println("2. Wyszukaj po Gatunku");
        System.out.println("3. Wyszukaj po Kolorze");
        System.out.println("4. Wyszukaj po cenie mniejszej niż");
        System.out.println("================================================");

        Scanner scanner = new Scanner(System.in);

        int searchMenuChoice = 0;
        try {
            searchMenuChoice = scanner.nextInt();
            if (searchMenuChoice < 1 || searchMenuChoice > 4) {
                throw new Exception("Wybierz prawidłową pozycję z menu");
            }
        } catch (Exception e) {
            System.out.println("Wybierz prawidłową pozycję z menu");
        }
        scanner.nextLine();
        return searchMenuChoice;
    }
}
